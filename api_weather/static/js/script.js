$(document).ready(function() {

    var form = $("#form_searching");

    memory = {};

    form.on("submit", function (event) {
        event.preventDefault();

        if ('timer' in memory){
            delTimer()
        }

        var city = $("#city").val();
        getWeatherInfo(city);
        timerId = setInterval(function() {
                    getWeatherInfo(city, timerId);
                     }, 10000);
        memory.timer = timerId

    });

    function delTimer(){
        clearInterval(memory.timer);
        delete memory.timer;
    }

    function getWeatherInfo(city) {
        $.get(
            "get_weather_info",
            {city: city},
            onAjaxSuccess
        );

        function onAjaxSuccess(data) {
            $("div.weather").empty().append(data);

            if ($("div").is(".dontFound")){
                delTimer()
            }
        }
    }
});

