# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
import requests
# Create your views here.


def home(request):
    name = "Weather API"
    return render(request, 'home.html', locals())


def get_weather_info(request):
    found_city = request.GET.get("city")
    try:
        weather_json = requests.get(
            'http://api.openweathermap.org/data/2.5/weather?appid=bfcd3d82a4a11392f0cdd21667abeaff&q=' + found_city
        ).json()

        city = weather_json['name']
        wind_speed = weather_json['wind']['speed']
        pressure = weather_json['main']['pressure']
        humidity = weather_json['main']['humidity']
        temp = weather_json['main']['temp'] - 273.15
        type_weather = weather_json['weather'][0]['main']
        icon_id = weather_json['weather'][0]['icon']

        city_weather = {'wind_speed': wind_speed,
                        'pressure': pressure,
                        'humidity': humidity,
                        'temp': temp,
                        'type_weather': type_weather,
                        'icon_id': icon_id,
                        'city': city,
                        }
        return render(request, 'city_weather.html', city_weather)
    except:
        return render(request, 'city_dont_found.html', {'city': found_city})

