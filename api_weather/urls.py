from django.conf.urls import url, include
from api_weather import views


urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^get_weather_info$', views.get_weather_info, name='get_weather_info'),
]